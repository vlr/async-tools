import { series } from "gulp";
import { fullBuild } from "./build";
import { runTestCoverage, runTestsEs5, runTestsEs6 } from "../test";

export const fullTest = series(
  fullBuild,
  runTestsEs5, runTestsEs6,
  runTestCoverage,
);
