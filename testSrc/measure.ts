export async function measure(action: () => Promise<unknown>): Promise<number> {
  const start = new Date().getTime();
  await action();
  return new Date().getTime() - start;
}
