import { waitFor } from "../src";
import { expect } from "chai";
import { measure } from "./measure";

describe("waitFor", function (): void {
  it("waits for signal", async function (): Promise<void> {
    // arrange
    const time = 5;
    const count = 5;
    // act
    let index = 0;
    const lasted = await measure(() => waitFor(() => ++index >= count, null, time));

    // assert
    expect(lasted + 1).greaterThan(time * count);
    expect(index).equals(count);
  });

  it("waits for timeout", async function (): Promise<void> {
    // arrange
    const time = 5;
    const count = 5;
    // act
    let index = 0;
    const lasted = await measure(() => waitFor(() => ++index >= count, 15, time));

    // assert
    expect(lasted + 1).lessThan(time * count);
    expect(index).lessThan(count);
  });
});
