import { range, areEquivalent } from "@vlr/array-tools";
import { expect } from "chai";
import { wait, runInGroups } from "../src";
import { measure } from "./measure";

describe("runInGroups", function (): void {
  it("runs sequential groups in parallel", async function (): Promise<void> {
    // arrange
    const input = range(16);

    // act
    let result: number[];
    const lasted = await measure(async () => result = await runInGroups(input, async (item) => {
      await wait(5);
      return item;
    }, 4));

    // assert
    expect(lasted).lessThan(40);
    expect(lasted).greaterThan(20);
    expect(areEquivalent(result, input)).equals(true);
  });
});
