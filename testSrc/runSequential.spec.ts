import { range } from "@vlr/array-tools";
import { expect } from "chai";
import { wait } from "../src";
import { runSequential } from "../src";
import { randomOf } from "./randomOf";

describe("runSequential", function (): void {
  it("runs sequential", async function (): Promise<void> {
    // arrange
    const input = range(5);

    // act
    const results = await runSequential(input, async (item) => {
      await wait(randomOf(5));
      return item;
    });

    // assert
    expect(results).deep.equal(input);
  });
});
