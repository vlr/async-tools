import { range, areEquivalent } from "@vlr/array-tools";
import { expect } from "chai";
import { wait, runParallel } from "../src";
import { measure } from "./measure";

describe("runParallel", function (): void {
  it("runs in parallel", async function (): Promise<void> {
    // arrange
    const input = range(15);

    // act
    let result: number[];
    const lasted = await measure(async () => result = await runParallel(input, async (item) => {
      await wait(5);
      return item;
    }));


    // assert
    expect(lasted).lessThan(12);
    expect(areEquivalent(result, input)).equals(true);
  });
});
