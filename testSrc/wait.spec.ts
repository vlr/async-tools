import { expect } from "chai";
import { wait } from "../src";
import { measure } from "./measure";

describe("wait", function (): void {
  it("returns waiting promise", async function (): Promise<void> {
    // arrange
    const time = 5;
    // act
    const lasted = await measure(() => wait(time));

    // assert
    expect(lasted + 1).greaterThan(time);
  });
});
