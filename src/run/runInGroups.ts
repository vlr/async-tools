import { splitByGroupsCount, flatMap } from "@vlr/array-tools";
import { RunAction } from "./runAction.type";
import { runSequential } from "./runSequential";
import { runParallel } from "./runParallel";

export async function runInGroups<TI, TR>(input: TI[], action: RunAction<TI, TR>, groups: number = 4): Promise<TR[]> {
  const inputGroups = splitByGroupsCount(input, groups);
  const results = await runParallel(inputGroups, group => runSequential(group, action));
  return flatMap(results);
}
