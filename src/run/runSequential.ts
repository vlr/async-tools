import { RunAction } from "./runAction.type";

export async function runSequential<TI, TR>(input: TI[], action: RunAction<TI, TR>): Promise<TR[]> {
  const results = [];
  for (let item of input) {
    const result = await action(item);
    results.push(result);
  }

  return results;
}
