# @vlr/async tools
Set of DRY tools, occasionally useful in asynchronous code

# wait
Waits for specified period in milliseconds. Basically this is setTimeout converted to Promise.

# waitFor
Waits until asynchronous condition is met. Can also specify a timeout and checking interval in milliseconds.

# runSequential
For each item in the array, sequentially runs provided asynchronous function. 

# runParallel
Encapsulation of Promise.all

# runInGroups
Splits provided array into groups. Items inside of a group run sequentially, groups are run in parallel. Returns array of results.

